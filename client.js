var Client = require('bittorrent-tracker')

var requiredOpts = {
  infoHash: new Buffer('012345678901234567890'), // hex string or Buffer
  peerId: new Buffer('01234567890123456789'), // hex string or Buffer
  announce: [], // lista de tracker server urls
  port: 6881 // torrent client port, (opcional para navegadores)
}

var optionalOpts = {
  getAnnounceOpts: function () {
    // Fornecer um retorno que irá chamar qualquer announce() é chamado
    // Internamente (on timer), ou pelo próprio usuário.
    return {
      uploaded: 0,
      downloaded: 0,
      left: 0,
      customParam: 'blah' //suporte para parâmetros personalizados
    }
  }
  // RTCPeerConnection é um objeto de configurção (only used in browser)
  rtcConfig: {},
  // User-Agent header para requisições2 http 
  userAgent: '',
  // Custom webrtc impl, useful in node to specify [wrtc](https://npmjs.com/package/wrtc)
  wrtc: {},
}

var client = new Client(requiredOpts)

client.on('error', function (err) {
  // fatal client error!
  console.log(err.message)
})

client.on('warning', function (err) {
  // Um tracker indisponivel ou envia dados errados pra o cliente.você porvavelmente pode ignora-lo.
  console.log(err.message)
})

// começa a obter peers para o tracker
client.start()

client.on('update', function (data) {
  console.log('got an announce response from tracker: ' + data.announce)
  console.log('number of seeders in the swarm: ' + data.complete)
  console.log('number of leechers in the swarm: ' + data.incomplete)
})

client.once('peer', function (addr) {
  console.log('found a peer: ' + addr) // 85.10.239.191:48623
})

// informar que o download foi comcluido (agora você é um seeder)
client.complete()

// força um tracker announce. irá desencadear eventos 'update' e talvez mais eventos 'peer'

client.update()

// Fornece parâmetros para o tracker
client.update({
  uploaded: 0,
  downloaded: 0,
  left: 0,
  customParam: 'blah' // suporte para parâmetros customizados
})

// para de obter peers para o tracker, deixa o swarm
client.stop()

// sem querer dixar o swarm (sem enviar a mensagem final de 'stop')
client.destroy()

// scrape
client.scrape()

client.on('scrape', function (data) {
  console.log('got a scrape response from tracker: ' + data.announce)
  console.log('number of seeders in the swarm: ' + data.complete)
  console.log('number of leechers in the swarm: ' + data.incomplete)
  console.log('number of total downloads of this torrent: ' + data.downloaded)
})
